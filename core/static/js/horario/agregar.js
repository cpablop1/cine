import { listar } from "./listar.js";

export function agregar() {
    let form = document.getElementById('form_guardar_horario');
    var data = new FormData(form);
    fetch('/guardar-horario/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if (res.res) {
            listar();
            console.log('Horario registrado correctamente.');
            form.reset();
            bootstrap.Modal.getInstance(document.getElementById('mdl_agregar_horario')).hide();
        } else {
            console.log('¡Hubo un error al registrar horario!');
        }
    });
}