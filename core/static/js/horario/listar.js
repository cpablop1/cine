export function listar() {
    fetch(`/listar-horario/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let fila = '';
            Array.from(r.data, (item, index) => {
                fila += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${item.inicia} - ${item.finaliza}</td>
                        <td>${item.fecha}</td>
                    `
                /* <td><button type="button" class="btn btn-info btn-sm" onclick="editProduct(${item.id})" id="${item.id}"><i class="bi bi-pencil-square"></i></button></td> */
            });
            document.getElementById('tbl_horario').childNodes[3].innerHTML = fila;
        })
}