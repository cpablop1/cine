import { agregar } from "./agregar.js";
import { listar } from "./listar.js";

window.onload = () => {
    listar();
}

document.getElementById('btn_agregar_horario').addEventListener('click', ()=> {
    new bootstrap.Modal(document.getElementById('mdl_agregar_horario')).show();
});

document.getElementById('btn_guardar_horario').addEventListener('click', ()=> {
    agregar();
});