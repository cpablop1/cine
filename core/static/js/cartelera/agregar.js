import { listar } from "./listar.js";

export function agregar() {
    let form = document.getElementById('form_guardar_cartelera');
    var data = new FormData(form);
    fetch('/guardar-cartelera/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if (res.res) {
            listar();
            console.log('Cartelera registrado correctamente.');
            form.reset();
            bootstrap.Modal.getInstance(document.getElementById('mdl_agregar_cartelera')).hide();
        } else {
            console.log('¡Hubo un error al registrar cartelera!');
        }
    });
}