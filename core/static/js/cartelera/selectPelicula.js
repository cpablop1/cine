
export function selectPelicula() {
    document.getElementById('pelicula').innerHTML = '';
    document.getElementById('pelicula').add(new Option(' ----- Seleccione película ----- ', ''))
    fetch('/listar-pelicula/').then((res) => {
        return res.json();
    }).then((res) => {
        Array.from(res.data, (item, index) => {
            document.getElementById('pelicula').add(new Option(item.nombre, item.id));
        });
    });
}