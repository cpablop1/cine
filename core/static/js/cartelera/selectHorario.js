
export function selectHorario() {
    document.getElementById('horario').innerHTML = '';
    document.getElementById('horario').add(new Option(' ----- Seleccione horario ----- ', ''))
    fetch('/listar-horario/').then((res) => {
        return res.json();
    }).then((res) => {
        Array.from(res.data, (item, index) => {
        document.getElementById('horario').add(new Option(`${item.fecha} | ${item.inicia} - ${item.finaliza}`, item.id));
        });
    });
}