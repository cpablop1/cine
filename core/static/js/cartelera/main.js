import { agregar } from "./agregar.js";
import { listar } from "./listar.js";
import { selectHorario } from "./selectHorario.js";
import { selectSala } from "./selectSala.js";
import  { selectPelicula } from './selectPelicula.js';

window.onload = () => {
    listar();
}

document.getElementById('btn_agregar_cartelera').addEventListener('click', ()=> {
    new bootstrap.Modal(document.getElementById('mdl_agregar_cartelera')).show();
    selectHorario();
    selectSala();
    selectPelicula();
});

document.getElementById('btn_guardar_cartelera').addEventListener('click', ()=> {
    agregar();
});