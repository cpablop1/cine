export function listar() {
    fetch(`/listar-cartelera/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let fila = '';
            Array.from(r.data, (item, index) => {
                fila += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${item.horario}</td>
                        <td>${item.fecha}</td>
                        <td>${item.sala}</td>
                    `
            });
            document.getElementById('tbl_cartelera').childNodes[3].innerHTML = fila;
        })
}