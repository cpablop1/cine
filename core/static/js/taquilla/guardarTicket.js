import { imprimirTicket } from '../ticket/imprimir.js';
import { listar } from './listar.js';

export function guardarTicket(id_cartelera) {
    var data = new FormData();
    data.append('id_cartelera', id_cartelera)
    fetch('/guardar-ticket/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if (res.res) {
            console.log('Ticket registrado correctamente.');
            bootstrap.Modal.getInstance(document.getElementById('mdl_confirmar')).hide();
            listar();
            imprimirTicket(res.ticket);
        } else {
            console.log('¡Hubo un error al registrar ticket!');
        }
    });
}