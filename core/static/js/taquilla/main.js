import { listar } from "./listar.js";
import { mostrarTicket } from "./mostrarTicket.js";
import { guardarTicket } from "./guardarTicket.js";

window.onload = () => {
    listar();
}

document.getElementById('taquilla').addEventListener('click', (e) => {
    let id = e.target.id;
    /* let cartelera = e.target.getAttribute('cartelera'); */
    console.log(id.length);
    if (id != 0 && id != 'taquilla') {
        if(parseInt(e.target.getAttribute('num_asientos')) == 0){
            alert('Sala llena!');
        } else{
            new bootstrap.Modal(document.getElementById('mdl_confirmar')).show();
            mostrarTicket(id, e.target.getAttribute('cartelera'));
        }
    }
})

document.getElementById('btn_confirmar').addEventListener('click', (e) => {
    console.log(e.target.getAttribute('id_cartelera'));
    guardarTicket(e.target.getAttribute('id_cartelera'));
});