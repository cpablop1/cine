export function listar() {
    fetch(`/listar-taquilla/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let pelicula = '';
            Array.from(r.data, (item, index) => {
              console.log(item);
                pelicula += `
                <div class="card m-1" style="max-width: 30%;">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="static/img/pelicula.png" class="img-fluid rounded-start">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title" role="button" id="${item.id}" num_asientos="${item.asientos}">${item.nombre}</h5>
                          <p class="card-text"><b>Clasifición:</b> ${item.clasi}</p>
                          <p class="card-text"><small class="text-muted"><b>Género:</b> ${item.genero}</small></p>
                          <p class="card-text"><small class="text-muted"><b>Precio:</b> Q.${item.precio}</small></p>
                          <hr>
                          <p class="card-text"><small class="text-muted"><b>Horario: </b>${item.horario}</small></p>
                          <p class="card-text"><small class="text-muted"><b>Sala: </b>${item.sala}</small></p>
                          <p class="card-text"><small class="text-muted"><b>Asientos disponibles: </b>${item.asientos}</small></p>
                          <p class="card-text"><small class="text-muted"><b>Fecha: </b>${item.fecha}</small></p>
                        </div>
                      </div>
                    </div>
                </div>
                    `
            });
            document.getElementById('taquilla').innerHTML = pelicula;
        })
}