export function mostrarTicket(id) {
    var data = new FormData();
    data.append('id', id)
    fetch('/mostrar-ticket/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        console.log(res.data[0]);
        document.getElementById('ver_pelicula').innerHTML = `<b>Película: </b>${res.data[0].pelicula}`;
        document.getElementById('ver_precio').innerHTML = `<b>Precio: </b>Q.${res.data[0].precio}`;
        document.getElementById('ver_horario').innerHTML = `<b>Horario: </b>${res.data[0].horario}`;
        document.getElementById('ver_sala').innerHTML = `<b>Sala: </b>${res.data[0].sala}`;
        /* document.getElementById('btn_confirmar').setAttribute('id_peli', res.data[0].id_peli); */
        document.getElementById('btn_confirmar').setAttribute('id_cartelera', res.data[0].id_cartelera);
    });
}