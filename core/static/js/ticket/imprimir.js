export function imprimirTicket(ticket) {
    var data = new FormData();
    data.append('ticket', ticket)
    fetch('/imprimir-ticket/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        let ticket = `
        <div id="imprimir_ticket">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
            <p class="text-center bg-primary"><b>Datos del Ticket</b></p>
            <p class="text-center"><b>Película: </b>${res.data[0].pelicula}</p>
            <p class="text-center"><b>Precio: </b>Q.${res.data[0].precio}</p>
            <p class="text-center"><b>Sala: </b>${res.data[0].sala}</p>
            <p class="text-center"><b>Emitida: </b>${res.data[0].fecha}</p>
        </div>`;

        let view = window.open(window.location.href, "PRINT");
        view.document.write(ticket);
        setTimeout(() => {
            view.print();
            view.close();
        }, 50);
    });
}