export function listar() {
    fetch(`/listar-ticket/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let fila = '';
            Array.from(r.data, (item, index) => {
                fila += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${item.pelicula}</td>
                        <td>${item.sala}</td>
                        <td>${item.monto}</td>
                        <td>${item.usuario}</td>
                        <td>${item.fecha}</td>
                    `
            });
            document.getElementById('tbl_ticket').childNodes[3].innerHTML = fila;
            document.getElementById('subtotal').innerHTML = `Q. ${r.subtotal}`
        })
}