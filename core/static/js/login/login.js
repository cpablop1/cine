export function login() {
    let form = document.getElementById('form_login');
    var data = new FormData(form);
    fetch('/iniciar-sesion/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if (res.res) {
            console.log('Sesión iniciada correctamente.');
            window.location.href = "/";
        } else {
            alert('¡Hubo un error al inciar sesión, vuelve a intentarlo!');
        }
    });
}