export function listar() {
    fetch(`/listar-sala/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let fila = '';
            Array.from(r.data, (item, index) => {
                fila += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${item.nombre}</td>
                        <td>${item.asientos}</td>
                        <td>${item.estado}</td>
                    `
            });
            document.getElementById('tbl_sala').childNodes[3].innerHTML = fila;
        })
}