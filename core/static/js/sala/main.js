import { agregar } from "./agregar.js";
import { listar } from "./listar.js";

window.onload = () => {
    listar();
}

document.getElementById('btn_agregar_sala').addEventListener('click', ()=> {
    new bootstrap.Modal(document.getElementById('mdl_agregar_sala')).show();
});

document.getElementById('btn_guardar_sala').addEventListener('click', ()=> {
    agregar();
});