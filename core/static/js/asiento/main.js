import { agregar } from "./agregar.js";
import { listar } from "./listar.js";
import { select } from "./select.js";

window.onload = () => {
    listar();
}

document.getElementById('btn_agregar_asiento').addEventListener('click', ()=> {
    new bootstrap.Modal(document.getElementById('mdl_agregar_asiento')).show();
    select();
});

document.getElementById('btn_guardar_asiento').addEventListener('click', ()=> {
    agregar();
});