export function listar() {
    fetch(`/listar-asiento/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let fila = '';
            Array.from(r.data, (item, index) => {
                fila += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${item.numero}</td>
                        <td>${item.fila}</td>
                        <td>${item.sala}</td>
                        <td>${item.estado}</td>
                    `
            });
            document.getElementById('tbl_asiento').childNodes[3].innerHTML = fila;
        })
}