import { listar } from "./listar.js";

export function agregar() {
    let form = document.getElementById('form_guardar_asiento');
    var data = new FormData(form);
    fetch('/guardar-asiento/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if (res.res) {
            listar();
            console.log('Asiento registrado correctamente.');
            form.reset();
            bootstrap.Modal.getInstance(document.getElementById('mdl_agregar_asiento')).hide();
        } else {
            console.log('¡Hubo un error al registrar asiento!');
        }
    });
}