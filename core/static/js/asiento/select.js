
export function select() {
    document.getElementById('sala').innerHTML = '';
    document.getElementById('sala').add(new Option(' ----- Seleccione sala ----- ', ''))
    fetch('/listar-sala/').then((res) => {
        return res.json();
    }).then((res) => {
        Array.from(res.data, (item, index) => {
            document.getElementById('sala').add(new Option(item.nombre, item.id));
        });
    });
}