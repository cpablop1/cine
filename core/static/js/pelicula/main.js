import { agregar } from "./agregar.js";
import { listar } from "./listar.js";

window.onload = () => {
    listar();
}

document.getElementById('btn_agregar_pelicula').addEventListener('click', ()=> {
    new bootstrap.Modal(document.getElementById('mdl_agregar_pelicula')).show();
});

document.getElementById('btn_guardar_pelicula').addEventListener('click', ()=> {
    agregar();
});