import { listar } from "./listar.js";

export function agregar() {
    let form = document.getElementById('form_guardar_pelicula');
    var data = new FormData(form);
    fetch('/guardar-pelicula/', {
        method: 'POST',
        body: data,
        headers: {
            'X-CSRFToken': document.querySelector('[name=csrfmiddlewaretoken]').value
        }
    }).then((res) => {
        return res.json();
    }).then((res) => {
        if (res.res) {
            listar();
            console.log('Película registrado correctamente.');
            form.reset();
            bootstrap.Modal.getInstance(document.getElementById('mdl_agregar_pelicula')).hide();
        } else {
            console.log('¡Hubo un error al registrar película!');
        }
    });
}