export function listar() {
    fetch(`/listar-pelicula/`)
        .then((r) => {
            return r.json();
        }).then((r) => {
            let fila = '';
            Array.from(r.data, (item, index) => {
                fila += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${item.nombre}</td>
                        <td>${item.genero}</td>
                        <td>${item.clasi}</td>
                    `
            });
            document.getElementById('tbl_pelicula').childNodes[3].innerHTML = fila;
        })
}