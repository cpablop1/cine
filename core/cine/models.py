from django.db import models
from django.contrib.auth.models import User

class Horario(models.Model):
    fecha = models.DateField()
    horario_inicial = models.TimeField()
    horario_final = models.TimeField()
    estado = models.BooleanField(default=True)

class Sala(models.Model):
    nombre = models.TextField(max_length=50)
    asientos = models.PositiveIntegerField(default=0)
    estado = models.BooleanField(default=True)

class Asiento(models.Model):
    estado = models.BooleanField(default=True)
    numero = models.TextField(max_length=5)
    fila = models.PositiveIntegerField(default=0)
    id_sala = models.ForeignKey(Sala, on_delete=models.CASCADE)

class Pelicula(models.Model):
    nombre = models.TextField(max_length=50)
    genero = models.TextField(max_length=50)
    clasificacion = models.TextField(max_length=50)
    duracion = models.TextField(max_length=5)

class PeliculaCartelera(models.Model):
    id_horario = models.ForeignKey(Horario, on_delete=models.CASCADE)
    id_sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    id_pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE)
    precio = models.FloatField(default=0)
    estado = models.BooleanField(default=True)
    entrada = models.PositiveIntegerField(default=0)

class Ticket(models.Model):
    id_cartelera = models.ForeignKey(PeliculaCartelera, on_delete=models.CASCADE)
    fecha_hora = models.DateTimeField(auto_now_add=True)
    monto = models.FloatField(default=0)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)