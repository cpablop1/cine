from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from cine.models import Horario, Sala, Asiento, PeliculaCartelera, Pelicula, Ticket
from django.contrib.auth.models import User

# Create your views here.
@login_required(login_url='ver_login')
def VerHorario(request):
    return render(request, 'horario/horario.html')

@login_required(login_url='ver_login')
def GuardarHorario(request):
    if request.method == 'POST':
        res = False
        fecha = request.POST.get("fecha")
        inicia = request.POST.get("inicia")
        finaliza = request.POST.get("finaliza")
        try:
            Horario.objects.create(
                fecha = fecha,
                horario_inicial = inicia,
                horario_final = finaliza
            )
            res = True
        except:
            res = False
    return JsonResponse({'res': res})

@login_required(login_url='ver_login')
def ListarHorario(request):
    # Listar todas la categorias
    horario = Horario.objects.all().order_by('id')
    data = {}
    data["data"] = []
    for hor in horario:
        data["data"].append(
            {"id": hor.id, "fecha": hor.fecha, "inicia": hor.horario_inicial, "finaliza": hor.horario_final}
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def VerSala(request):
    return render(request, 'sala/sala.html')

@login_required(login_url='ver_login')
def GuardarSala(request):
    if request.method == 'POST':
        res = False
        descripcion = request.POST.get("descripcion")
        asientos = request.POST.get('asientos')
        try:
            Sala.objects.create(
                nombre = descripcion,
                asientos = asientos
            )
            res = True
        except:
            res = False
    return JsonResponse({'res': res})

@login_required(login_url='ver_login')
def ListarSala(request):
    # Listar todas la salas
    sala = Sala.objects.all().order_by('id')
    data = {}
    data["data"] = []
    for sal in sala:
        estado = 'Disponible' if sal.estado else 'No disponible'
        print(estado)
        data["data"].append(
            {"id": sal.id, "nombre": sal.nombre, "asientos": sal.asientos, 'estado': estado}
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def VerAsiento(request):
    return render(request, 'asiento/asiento.html')

@login_required(login_url='ver_login')
def GuardarAsiento(request):
    if request.method == 'POST':
        res = False
        numero = request.POST.get("numero")
        fila = request.POST.get("fila")
        sala = request.POST.get("sala")
        try:
            Asiento.objects.create(
                numero = numero,
                fila = fila,
                id_sala = Sala.objects.get(id=sala)
            )
            res = True
        except:
            res = False
    return JsonResponse({'res': res})

@login_required(login_url='ver_login')
def ListarAsiento(request):
    # Listar todas los asientos
    asiento = Asiento.objects.all().order_by('id')
    data = {}
    data["data"] = []
    for asi in asiento:
        estado = 'Disponible' if asi.estado else 'No disponible'
        data["data"].append(
            {"id": asi.id, "numero": asi.numero, "fila": asi.fila, "sala": asi.id_sala.nombre, "estado": estado}
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def VerCartelera(request):
    return render(request, 'cartelera/cartelera.html')

@login_required(login_url='ver_login')
def GuardarCartelera(request):
    if request.method == 'POST':
        res = False
        horario = request.POST.get("horario")
        sala = request.POST.get("sala")
        pelicula = request.POST.get("pelicula")
        precio = request.POST.get("precio")
        asientos = Asiento.objects.filter(id_sala=sala).count()
        try:
            sala = Sala.objects.get(id=sala)
            PeliculaCartelera.objects.create(
                id_horario = Horario.objects.get(id=horario),
                id_sala = sala,
                id_pelicula = Pelicula.objects.get(id=pelicula),
                precio = precio,
                entrada = asientos
            )
            res = True
        except:
            res = False
    return JsonResponse({'res': res})

@login_required(login_url='ver_login')
def ListarCartelera(request):
    # Listar todas los cartelera
    cartelera = PeliculaCartelera.objects.all().order_by('id')
    data = {}
    data["data"] = []
    for car in cartelera:
        data["data"].append(
            {"id": car.id, "horario": f"{car.id_horario.horario_inicial} - {car.id_horario.horario_final}", "fecha": car.id_horario.fecha, "sala": car.id_sala.nombre}
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def VerPelicula(request):
    return render(request, 'pelicula/pelicula.html')

@login_required(login_url='ver_login')
def GuardarPelicula(request):
    if request.method == 'POST':
        res = False
        nombre = request.POST.get("nombre")
        genero = request.POST.get("genero")
        clasificacion = request.POST.get("clasificacion")
        duracion = request.POST.get("duracion")
        try:
            Pelicula.objects.create(
                nombre = nombre,
                genero = genero,
                clasificacion = clasificacion,
                duracion = duracion
            )
            res = True
        except:
            res = False
    return JsonResponse({'res': res})

@login_required(login_url='ver_login')
def ListarPelicula(request):
    # Listar todas las peliculas
    pelicula = Pelicula.objects.all().order_by('id')
    data = {}
    data["data"] = []
    for pel in pelicula:
        data["data"].append(
            {"id": pel.id, "nombre": pel.nombre, "genero": pel.genero, "clasi": pel.clasificacion}
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def Taquilla(request):
    return render(request, 'taquilla/taquilla.html')

@login_required(login_url='ver_login')
def ListarTaquilla(request):
    # Listar todas las peliculas para mostrar en taquilla
    cartelera = PeliculaCartelera.objects.all().order_by('id')
    data = {}
    data["data"] = []
    for car in cartelera:
        data["data"].append(
            {
                'id': car.id,
                'nombre': car.id_pelicula.nombre,
                'genero': car.id_pelicula.genero,
                'clasi': car.id_pelicula.clasificacion,
                'precio': car.precio,
                'horario': f'{car.id_horario.horario_inicial} - {car.id_horario.horario_final}',
                'sala': car.id_sala.nombre,
                'asientos': car.id_sala.asientos,
                'fecha': car.id_horario.fecha
            }
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def MostrarTicket(request):
    if request.method == 'POST':
        cartelera = request.POST.get("id")
        ver_cart = PeliculaCartelera.objects.get(id=cartelera)
        data = {}
        data["data"] = []
        data["data"].append(
            {
                'id_peli': ver_cart.id_pelicula.id,
                'id_cartelera': ver_cart.id,
                'pelicula': ver_cart.id_pelicula.nombre,
                'precio': ver_cart.precio,
                'horario': f'{ver_cart.id_horario.horario_inicial} - {ver_cart.id_horario.horario_final}',
                'sala': ver_cart.id_sala.nombre
            }
        )
    return JsonResponse(data)

@login_required(login_url='ver_login')
def GuardarTicket(request):
    if request.method == 'POST':
        """ id_peli = request.POST.get('id_peli')
        pelicula = Pelicula.objects.get(id=id_peli) """
        id_cartelera = request.POST.get('id_cartelera')
        print(f'id_cartelera: {id_cartelera}')
        cartelera = PeliculaCartelera.objects.get(id=id_cartelera)
        sala = Sala.objects.get(id=cartelera.id_sala.id)
        res = False
        ticket = 0
        try:
            ticket = Ticket.objects.create(
                id_cartelera = cartelera,
                monto = cartelera.precio,
                usuario = User.objects.get(id=request.user.id)
            )
            res = True
            sala.asientos = sala.asientos - 1
            sala.save()
            ticket = ticket.id
        except:
            res = False
    return JsonResponse({'ticket': ticket, 'res': res})

@login_required(login_url='ver_login')
def VerTicket(request):
    return render(request, 'ticket/ticket.html')

@login_required(login_url='ver_login')
def ListarTicket(request):
    # Listar todas los tickest
    ticket = Ticket.objects.all().order_by('id')
    data = {}
    data["data"] = []
    subtotal = 0
    for tic in ticket:
        subtotal = subtotal + float(tic.monto)
        data["data"].append(
            {
                "id": tic.id, 
                "pelicula": tic.id_cartelera.id_pelicula.nombre, 
                "sala": tic.id_cartelera.id_sala.nombre, 
                "monto": tic.monto, 
                "usuario": tic.usuario.username, 
                "fecha": tic.fecha_hora.strftime("%A, %d de %B de %Y a las %I:%M %p")
            }
        )
    data['subtotal'] = subtotal
    return JsonResponse(data)

@login_required(login_url='ver_login')
def ImprimirTicket(request):
    if request.method == 'POST':
        id_ticket = request.POST.get('ticket')
        ticket = Ticket.objects.get(id=id_ticket)
        """ pelicula = Pelicula.objects.get(id_cartelera=ticket.id_cartelera) """
        data = {}
        data['data'] = []
        data["data"].append(
            {
                'pelicula': ticket.id_cartelera.id_pelicula.nombre,
                'precio': ticket.monto,
                'sala': ticket.id_cartelera.id_sala.nombre,
                'fecha': ticket.fecha_hora
            }
        )
    return JsonResponse(data)

def VerLogin(request):
    return render(request, 'login/login.html')

def Login(request):
    if request.method == "POST":
        usuario = ("" if len(request.POST["usuario"].strip()) == 0 else request.POST["usuario"].strip())
        clave = ("" if len(request.POST["clave"].strip()) == 0 else request.POST["clave"].strip())
        user = authenticate(request, username=usuario, password=clave)
        if user is not None:
            login(request, user)
            return JsonResponse({"res": True})
        else:
            return JsonResponse({"res": False})
        
def LogOut (request):
    logout(request)
    return redirect('ver_login')